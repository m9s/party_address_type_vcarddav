# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Party Address Type vCardDAV',
    'name_de_DE': 'Parteien Adressen Typen vCardDAV',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds support of address types for parties with vCardDAV
    ''',
    'description_de_DE': '''
    - Fügt Unterstützung von Adresstypen für Parteien mit vCardDAV hinzu.
    ''',
    'depends': [
        'party_address_type',
        'party_vcarddav',
    ],
    'xml': [
    ],
    'translation': [
    ],
}
