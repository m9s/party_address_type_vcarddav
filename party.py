# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView


class Address(ModelSQL, ModelView):
    _name = 'party.address'

    def vcard2values(self, adr):
        vals = super(Address, self).vcard2values(adr)
        if hasattr(adr, 'type_paramlist'):
            paramlist = [x.lower() for x in adr.type_paramlist]
            if 'home' in paramlist:
                vals['home'] = True
            if 'pref' in paramlist:
                vals['preferred'] = True
        return vals

    def _address2type_paramlist(self, address, vcard_adr):
        res = super(Address, self)._address2type_paramlist(address, vcard_adr)

        if address.preferred:
            if 'pref' not in res:
                res.append('pref')
        elif 'pref' in res:
            res.remove('pref')

        if address.home:
            if 'home' not in res:
                res.append('home')
            if 'work' in res:
                res.remove('work')
        else:
            if 'work' not in res:
                res.append('work')
            if 'home' in res:
                res.remove('home')
        return res

Address()

